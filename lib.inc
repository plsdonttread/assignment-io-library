section .text

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi], 0
        je .end
        inc rax
        inc rdi
        jmp .loop
    .end:    
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov rdx, rax
    mov rsi, rdi

    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    push 0xA
    mov rdi, [rsp]
    call print_char
    pop rax
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp
    mov rbp, rsp 
    push 0
    xor rax, rax
    mov rax, rdi
    mov rsi, 10
    .loop:
        xor rdx, rdx
        div rsi
        add rdx, '0'
        dec rsp
        mov [rsp], dl ;lower 8 bits of rdx
        test rax, rax
        jnz .loop
    mov rdi, rsp
    call print_string
    leave
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns .print
    .sign_job:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        not rdi
        inc rdi
    .print:
        call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals: ;rdi rsi  rdx rcx
    mov rax, 1
    .loop:
        mov dl, byte [rdi]
        mov cl, byte [rsi]
        inc rdi
        inc rsi
        cmp dl, cl
        jne .ne
        test dl, dl
        jz .end
        jmp .loop
    .ne: 
        xor rax, rax
    .end:
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    push rax
    mov rax, 0
    mov rdi, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    mov rax, [rsp]
    add rsp, 8

    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    mov r9, rsi
    dec r9
    xor rdx, rdx
    .end_check:
        test rdx, rdx
        jz .read
        cmp rdx, rsi
        jl .end_good
    .read:
        test r9, r9
        jz .fail

        push rdx
        call read_char
        pop rdx


        cmp rax, 0x20
        je .end_check
        cmp rax, 0x9
        je .end_check
        cmp rax, 0xA
        je .end_check
        test rax, rax
        jz .end_good

        mov [r8+rdx], al
        inc rdx
        dec r9

        jmp .read
    .fail:
        xor rax, rax
        ret
    .end_good:
        mov [r8+rdx], byte 0  
        mov rax, r8
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint: ;rdi rsi rdx rcx r8 r9
    xor rax, rax
    xor r9, r9
    mov r8, 10
    .loop:
        mov rcx, [rdi+r9]
        cmp cl, '0'
        jb .end
        cmp cl, '9'
        ja .end
        mul r8
        add al, cl
        sub al, '0'
        inc r9
        jmp .loop
    .end:
        mov rdx, r9
        ret



; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov al, byte [rdi]
    push rax
    cmp al, '-'
    jne .as_uint
    inc rdi
    .as_uint:
        call parse_uint
        pop rdi
        test rdx, rdx
        jz .end
        cmp dil, '-'
        jne .end
        not rax
        inc rax
        inc rdx
    .end:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rdx, rax
        je .end_bad
        inc rax
        mov rcx, [rdi]
        mov [rsi], rcx
        cmp byte [rdi], 0
        je .end
        inc rdi
        inc rsi
        jmp .loop
    .end_bad:
        xor rax, rax
    .end:
        ret
